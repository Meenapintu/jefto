<!DOCTYPE html>
<html lang="en">
<head>
  <title>Jefto.com </title>
  <link rel="icon" type="image/png" href="def_img/favicon.png"/>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="author" content="">
  <meta name="devloper" content="">
  <meta name="copyright" content="jefto.com">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> 
  <link rel="stylesheet" href="materialize/css/materialize.min.css"  media="screen,projection" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

  <link rel="stylesheet" href="forms.css">

  <script src="materialize/js/jquery-2.1.4.min.js"></script> 
 <?php  include_once('ganly.php');?>
</head>
<body>
  <nav>
    <div class="nav-wrapper">
      <a href="/" class="brand-logo"><img src="def_img/jefto.png" id='logo' style="height:100%; max-height:64px;max-width:171px;position:relative" /></a>
      <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
      <ul class="right hide-on-med-and-down">
        <li><a href="form.php">Submit Event</a></li>
	<li><a href="browse.php">Browse Events</a></li>
        <li><a href="index.php#whatwedo">What We Do </a></li>
        <li><a href="#contact">Contact</a></li>
      </ul>
      <ul class="side-nav" id="mobile-demo">
         <li><a href="form.php">Submit Event</a></li>
	<li><a href="browse.php">Browse Events</a></li>
         <li><a href="index.php#whatwedo">What We Do </a></li>
        <li><a href="#contact">Contact</a></li>
      </ul>
    </div>
  </nav>
